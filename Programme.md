# Programme

Le grand principe : une première partie non technique, la suite est orientée technique et pratique.

## Nos a-prioris sur le Logiciel Libre

(aka trollons sur le libre)

* Quels sont les reproches formulés à l'encontre du logiciel libre (à tord ou à raison)
* Qu'est-ce qui ne fonctionne pas dans le logiciel libre

### Configuration de son git

1. Définir son identité : `git config --global user.name "Prénom NOM"`
2. Définir son courriel : `git config --global user.email "moi@moncourriel.Fr"`

### Git clone en anonyme (CLI)

1. git clone d'un repos en anonyme
1. git log -5
1. git commit

### Workflow de travail sur D&N

* dérouler le worlflow: issue, ouvrir MR, coder, merge master dans branche, soumission MR

### Comment mettre à jour son fork

* ajouter un remote upstream sur son repos local puis push sur son origin

### Environnements de dev / test / prod

* poser les idées
* amener les runners